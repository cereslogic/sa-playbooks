Random ideas
------------

1. Add better ENV vars for Ruby GC settings, e.g. RUBY_GC_MALLOC_LIMIT: 40000000 and RUBY_HEAP_MIN_SLOTS: 800000
RUBY_GC_MALLOC_LIMIT - about 50MB
RUBY_HEAP_MIN_SLOTS - $(( 408*1500 )) ??? or 408 * 1000 or 408 * 2000
RUBY_FREE_MIN
1. The workers do not pick up new ENV vars unless manually stopped/started (because the ENV vars are set in their init scripts I think, simply killing or restarting is not enough)
1. The unicorn service is not starting back up automatcially after reboot
1. Filter out health_check calls in the logs? They are going into nginx access.log and unicorn_stdout log right now. NOTE: maybe with rotated/compressed logs, this will not matter
1. I know nothing about logwatch - what settings should be changed?
1. Use jemalloc and Unicorn OOBGC settings
1. The current setup deploys to both app servers and the worker server at the same time. Which means we would rely on unicorn zero-downtime deploys. I started this way because it seemed far less complicated than taking app1 off, deploying, bring up, take app2 off, deploy... etc. It may work fine, seems to be fine so far.

Post Migration cleanup
----------------------
1. Fix resque connection handling: http://stackoverflow.com/questions/9961044/postgres-error-on-heroku-with-resque and https://devcenter.heroku.com/articles/forked-pg-connections - need to disconnect in the before_fork I think
1. DB log - "2014-07-28 14:40:21 EDT LOG:  could not receive data from client: Connection reset by peer" Is this just heroku being stupid?
1. Revert Redis and Postgres binding/listen settings and firewall changes (local/private only now)
1. Remove heroku gems and proxy hacks
1. will config.force_ssl work now?
1. cron to move production to staging nightly (database)
1. Upgrade to rails 4
