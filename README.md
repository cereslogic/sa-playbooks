playbooks
=========

Ansible playbooks for Sure Address

1. For SSH to work, you may need to disable Host Key checking. I didn't really want to do this with production. Instead, you can simply manually ssh to each server before running the first ansible playbook. This way, SSH will ask you to verify the host, and add it to your known\_hosts file.  
1. Bootstrap new servers  
  `ansible-playbook bootstrap.yml -i staging --ask-pass`
1. Reboot the servers  
  `ansible-playbook reboot.yml -i staging`
1. Provision all servers  
  `ansible-playbook provision.yml -i staging`
1. Deploy app (branch develop, prod uses master by default)  
  `deploy_staging`  
  `ansible-playbook deploy.yml -i staging`
1. Deploy app with custom branch  
  `deploy_staging branch_name`  
  `ansible-playbook deploy.yml --extra-vars "deploy_branch=your_branch_name" -i staging`
1. Turn on maintenance mode  
  `maintenance_staging on` or `maintenance_production on` (pass off to turn off maintenance mode)

Deploy
------

NOTE: need to make a new ansible task for this. As well as make the entier deploy faster

1. git co master
1. git pull
1. git merge --no-ff develop
1. git tag 2.0.xx
1. git push --tags
1. git push
1. `deploy_production`
1. Optionally, turn on maintenance mode if you are doing something that will take down services  
  `maintenance_staging on` or `maintenance_production on` (pass off to turn off maintenance mode)
1. create a release in pivotal with v2.2.xx  
  finish that release so that all tickets included are finished/accepted above

Update staging DB
-----------------

I made a role called `dbtransfer` that can be used to DROP, CREATE, and restore the DB.  
You need to ensure the file `db_dump.pgdump` exists in the root of this directory.  
Then, simply run `ansible-playbook dbtransfer.yml -i staging`  
This should not work for production, since that would be very bad.  

1. Stops the workers/scheduler/app servers
1. Uploads the dump file
1. DROP and CREATE the DB
1. CREATE the needed extensions
1. Setup database permissions
1. Runs pg\_restore

SSH Keys
--------

1. Add your public key to `roles/authorized_keys/files`
2. Add the file lookup in `roles/authorized_keys/defaults/main.yml`
3. Have someone with access run the playbook to upload to all servers  
  `ansible-playbook authorized_users.yml -i staging`
4. Add the `ssh_config` contents to your own ~/.ssh/config file for easy SSH'ing

Environment Variables
---------------------
We are using https://github.com/bkeepers/dotenv-deployment for setting ENV vars in staging/production.  
The way it works is by using ansible variables to create a .env file in the root of the Rails app.  
This file is automatically loaded by dotenv-deployment and all key/value pairs added to the ENV.

However, you do still need to specify the RAILS\_ENV and RACK\_ENV manually before dotenv is loaded.  
I still use ansible to copy the env\_vars to appropriate script files, plus in the .bashrc so it works when you SSH in.  
These ENV vars shouldn't change, but if you need others like this, you can use

`ansible-playbook add_env.yml -i staging`

to update them.

Deploy Overview
--------
The deploy playbook follows the capistrano formula. It looks like this:

1. Maintain a bare copy of the repo
2. Clone from bare repo into tmp directory
3. Configure app (setup symlinks, bundle, migrate, precompile assets, etc.)
4. Move tmp directory to releases folder
5. Symlink current folder to new directory in releases
6. Restart services

Security
--------
Since so much other super secure stuff is already in this repo, might as well add one more:

qa root password: UoWsyUCCrVpr7m8gqLHHQiik
staging root password: fusD5vXYMzgsR4PQECnlv0Zr  
production root password: eK5fA8UFjitl0YWHcIRwb3nM

These passwords I think are useless though since root SSH is disabled, and the deployer user can do sudo things with no password

Credits
-------
lots of code/inspiration taken from https://github.com/nicolai86/ansible-rails-deployment
